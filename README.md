# Simple Website

### Getting Started
 Clone the repo and open index.html
 
    git clone https://bitbucket.org/antonija_simic/infinum_project
 
 
 If you want to make changes, open the partial file you wish to change, save it and run gulp

    gulp

 
### Features
* Responsive Layout
* Pixel Perfect
* Smooth Scroll
* [Custom Hover Effects](http://tonkec.github.io/text_hover_effects/index.html)
* [Preloader](http://webdesign.tutsplus.com/tutorials/creating-a-collection-of-css3-animated-pre-loaders--cms-21978)